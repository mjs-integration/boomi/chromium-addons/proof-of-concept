/*------Global------*/
//Variables
var testComponent = 'bd86a8f4-954c-48ce-8066-55978a124bfc'
var licenceActive = false
var boomiAccount = 'boltonclarke-PMZV2K'







/*function param(name) {
    alert('called')
    var urlpath = $(location).attr('href')
    return (urlpath.split(name + '=')[1] || '').split('&')[0];
}*/

/*------Functions used to check and define processes that will be used to validate if in use by other developers------*/


//Function to retrieve the current URL parameters and split them into each unique record
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = $(location).attr('href'),
        sURLVariables = sPageURL.split(';'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};


//Function used to advise alert if component process is locked
function boomiaccessAltert(componentName){

    swal({
        title: 'Process Access Warning',
        text: 'The Integration Process ' + componentName + ' is currently being viewed/edited by another developer.',
        icon:'warning',
        showConfirmButton: true,
        closeOnConfirm: true,
        confirmButtonText: 'I understand',
        confirmButtonColor: '#bd0010',
    });


}


//Function to get the componets from the URL as pass them on to the next function to be split and returned
function componentUpdate(){

 var apiComponents = getUrlParameter("components");


}



//Polling function that will be called when the page loads and start the check process
function componentPolling(){
    
    var doomiacount = getUrlParameter("accountId");
    if (doomiacount == boomiAccount) {

       componentUpdate()
    }
       setTimeout(componentPolling, 5500);
   }



